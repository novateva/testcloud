const admin = require("firebase-admin");

admin.initializeApp();
const db = admin.database();

const getUserData = key => db.ref(`/users/${key}`).once("value");

const UpdateData = (key, body) =>
  db
    .ref(`/users/${key}`)
    .once("value")
    .then(snapshot => {
      if (!snapshot.exists()) {
        throw new Error("User Not Found!");
      }
      return "";
    })
    // eslint-disable-next-line promise/always-return
    .then(() => {
      db.ref(`/users/${key}`).update(body);
    });

module.exports = {
  admin,
  UpdateData,
  db,
  getUserData
};
