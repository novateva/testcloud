const express = require("express");

const { db } = require("./common");

const app = express();

app.post(
  "/",
  /**
   * Summary. Create a pocket with request body's data.
   */
  (req, res, next) => {
    if (!req.body.billeteraID) {
      setImmediate(() => next(new Error("Wallet has no id!")));
      return;
    }
    db.ref(`/bolsillo`)
      .orderByChild("billeteraID")
      .equalTo(req.body.billeteraID)
      .limitToLast(1)
      .once("value")
      .then(snapshot => {
        if (snapshot.exists()) {
          throw new Error("Wallet already exists");
        }
        return "";
      })
      .then(() => db.ref(`/bolsillo`).push(req.body))
      .then(val => res.send(val))
      .catch(next);
  }
);

app.get(
  "/:randomuid",
  /**
   * Summary. Get a pocket.
   */
  (req, res, next) => {
    db.ref(`/bolsillo/${req.params.randomuid}`)
      .once("value")
      .then(snapshot => {
        if (snapshot.exists()) {
          return res.send(snapshot.val());
        }
        return res.status(404).send("Not found!");
      })
      .catch(next);
  }
);

app.get(
  "/",
  /**
   * Summary. Get all pockets.
   */
  (req, res, next) => {
    db.ref(`/bolsillo`)
      .once("value")
      .then(snapshot => {
        if (snapshot.exists()) {
          return res.send(snapshot.val());
        }
        return res.status(404).send("Not found!");
      })
      .catch(next);
  }
);

app.patch(
  "/:randomuid",
  /**
   * Summary. Merge a pocket's data with request body's data.
   */
  (req, res, next) => {
    db.ref(`/bolsillo/${req.params.randomuid}`)
      .once("value")
      .then(snapshot => {
        if (snapshot.exists()) {
          return "";
        }
        throw new Error("Wallet does not exist!");
      })
      .then(() => db.ref(`/bolsillo/${req.params.randomuid}`).update(req.body))
      .then(val => res.send(val))
      .catch(next);
  }
);

app.delete(
  "/:randomuid",
  /**
   * Summary. Remove a pocket.
   */
  (req, res, next) => {
    db.ref(`/bolsillo/${req.params.randomuid}`)
      .remove(err => {
        if (err) {
          throw err;
        }
        return "";
      })
      .then(() => res.sendStatus(200))
      .catch(next);
  }
);

module.exports = app;
