const express = require("express");

const { db } = require("./common");

const app = express();

app.get("/", (req, res, next) => {
  db.ref(`/movimiento`)
    .once("value")
    .then(snapshot => {
      if (snapshot.exists()) {
        return res.json(snapshot.val());
      }
      return res.status(404).json({
        ok: false,
        message: "Not found"
      });
    })
    .catch(next);
});

app.get(
  "/:movimientoKey",
  /**
   * Summary. Get all pockets from a movimiento.
   */
  (req, res, next) => {
    db.ref(`/movimiento/${req.params.movimientoKey}`)
      .once("value")
      .then(snapshot => {
        if (snapshot.exists()) {
          return res.send(snapshot.val());
        }
        return res.status(404).json({
          ok: false,
          message: "Not found"
        });
      })
      .catch(next);
  }
);

app.post("/", (req, res) => {
  db.ref(`/movimiento`)
    .push(req.body)
    .then(val =>
      res.json({
        ok: true,
        movimiento: val
      })
    )
    .catch(err =>
      res.status(400).json({
        ok: false,
        err
      })
    );
});

app.patch("/:movimientoKey", (req, res) => {
  db.ref(`/movimiento`)
    .child(req.params.movimientoKey)
    .once("value")
    .then(
      snapshot =>
        new Promise((resolve, reject) =>
          snapshot.exists() ? resolve(true) : reject(new Error("Not found"))
        )
    )
    // eslint-disable-next-line promise/always-return
    .then(() => {
      // eslint-disable-next-line promise/no-nesting
      db.ref(`/movimiento`)
        .child(req.params.movimientoKey)
        .update(req.body)
        .then(() =>
          res.json({
            ok: true
          })
        )
        .catch(err =>
          res.status(400).json({
            ok: false,
            err: err.message
          })
        );
    })
    .catch(err =>
      res.status(404).json({
        ok: false,
        err: err.message
      })
    );
});

app.delete("/:movimientoKey", (req, res, next) => {
  db.ref(`/movimiento/${req.params.movimientoKey}`)
    .remove(err => {
      if (err) {
        throw err;
      }
      return "";
    })
    .then(() =>
      res.json({
        ok: true
      })
    )
    .catch(() =>
      res.json({
        ok: false
      })
    );
});
module.exports = app;
