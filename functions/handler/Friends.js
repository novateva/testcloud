const express = require("express");

const { db, getUserDataWithKey } = require("./common");

const app = express();

app.post(
  "/:userKey",
  /**
   * Summary. Make friends to two users.
   */
  (req, res, next) => {
    if (!req.body.userKey) {
      setImmediate(() => next(new Error("No user Key specified!")));
      return;
    }

    if (req.body.userKey === req.params.userKey) {
      setImmediate(() => next(new Error("Users are the same!")));
      return;
    }

    db.ref(`/Friends/${req.params.userKey}`)
      .orderByKey()
      .equalTo(req.body.userKey)
      .limitToLast(1)
      .once("value")
      .then(snapshot => {
        if (snapshot.exists()) {
          // BIG NOTE: assuming this post function has been used on all Friends listed till now
          throw new Error("Users are already friends!");
        }
        return "";
      })
      .then(() =>
        Promise.all([
          getUserDataWithKey(req.params.userKey),
          getUserDataWithKey(req.body.userKey)
        ])
      )
      .then(([sourceUser, destinUser]) => {
        if (!sourceUser.exists()) {
          throw new Error("Friend source does not exists");
        }

        if (!destinUser.exists()) {
          throw new Error("Friend to add does not exists");
        }

        db.ref(`/Friends/${req.body.userKey}`).update(sourceUser.val());
        return db
          .ref(`/Friends/${req.params.userKey}`)
          .update(destinUser.val());
      })
      .then(val => res.send(val))
      .catch(next);
  }
);

app.get(
  "/:userKey/:userKey2",
  /**
   * Summary. Get a friend from a  user.
   */
  (req, res, next) => {
    db.ref(`/Friends/${req.params.userKey}/${req.params.userKey2}`)
      .once("value")
      .then(snapshot => {
        if (snapshot.exists()) {
          return res.send(snapshot.val());
        }
        return res.status(404).send("Not found!");
      })
      .catch(next);
  }
);

app.get(
  "/:userKey",
  /**
   * Summary. Get all friends of an user.
   */
  (req, res, next) => {
    db.ref(`/Friends/${req.params.userKey}`)
      .once("value")
      .then(snapshot => {
        if (snapshot.exists()) {
          return res.send(snapshot.val());
        }
        return res.status(404).send("Not found!");
      })
      .catch(next);
  }
);

app.get(
  "/",
  /**
   * Summary. Get all friends of all users.
   */
  (req, res, next) => {
    db.ref(`/Friends`)
      .once("value")
      .then(snapshot => {
        if (snapshot.exists()) {
          return res.send(snapshot.val());
        }
        return res.status(404).send("Not found!");
      })
      .catch(next);
  }
);

app.patch(
  "/:userKey/:userKey2",
  /**
   * Summary. Update data of an user's friend.
   */
  (req, res, next) => {
    db.ref(`/Friends/${req.params.userKey}/${req.params.userKey2}`)
      .once("value")
      .then(snapshot => {
        if (!snapshot.exists()) {
          throw new Error("Friend does not exist!");
        }
        return "";
      })
      .then(() =>
        db
          .ref(`/Friends/${req.params.userKey}/${req.params.userKey2}`)
          .update(req.body)
      )
      .then(val => res.send(val))
      .catch(next);
  }
);

app.delete(
  "/:userKey/:userKey2",
  /**
   * Summary. Delete a friend from a user, it goes on both ways.
   */
  (req, res, next) => {
    db.ref(`/Friends/${req.params.userKey}/${req.params.userKey2}`)
      .remove()
      .then(() =>
        db.ref(`/Friends/${req.params.userKey2}/${req.params.userKey}`).remove()
      )
      .then(() => res.sendStatus(200))
      .catch(next);
  }
);

app.delete(
  "/:userKey",
  /**
   * Summary. Delete all friends of an user, it also deletes user from other friend lists.
   */
  (req, res, next) => {
    db.ref(`/Friends`)
      .once("value")
      .then(snapshot => {
        const arr = [];

        snapshot.forEach(snap2 => {
          arr.push(snap2.key);
          return false;
        });

        return arr;
      })
      .then(keys =>
        keys.map(key =>
          db.ref(`/Friends/${key}/${req.params.userKey}`).remove()
        )
      )
      .then(queries => Promise.all(queries))
      .then(() => db.ref(`/Friends/${req.params.userKey}`).remove())
      .then(() => res.sendStatus(200))
      .catch(next);
  }
);

module.exports = app;
