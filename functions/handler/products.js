/* eslint-disable promise/no-nesting */
const express = require("express");
// eslint-disable-next-line import/no-unresolved
const UUID = require("uuid-v4");

const path = require("path");
const os = require("os");
const fs = require("fs");
// eslint-disable-next-line import/no-unresolved
const Busboy = require("busboy");
const { db, bucket } = require("./common");

const app = express();

app.post(
  "/:userKey",
  /**
   * @brief Add product to user with ID userKey.
   */
  (req, res, next) => {
    if (!req.params.userKey) {
      setImmediate(() => next(new Error("No user Key specified!")));
      return;
    }

    const busboy = new Busboy({ headers: req.headers });
    let uploadData = null;
    let origFileName;

    busboy.on("file", (fieldname, file, filename, encoding, mimetype) => {
      origFileName = filename;
      const filepath = path.join(os.tmpdir(), filename);
      uploadData = { file: filepath, type: mimetype };
      file.pipe(fs.createWriteStream(filepath));
    });

    let obj;
    // This callback will be invoked after all uploaded files are saved.
    busboy.on("finish", () => {
      obj = JSON.parse(fs.readFileSync("/tmp/wut.json", "utf8"));
      const uuid = UUID();

      bucket
        .upload(uploadData.file, {
          uploadType: "media",
          metadata: {
            metadata: {
              contentType: uploadData.type,
              firebaseStorageDownloadTokens: uuid,
              predefinedAcl: "publicRead"
            }
          }
        })
        .then(results => {
          const img = `https://firebasestorage.googleapis.com/v0/b/${
            bucket.name
          }/o/${encodeURIComponent(results[0].name)}?alt=media&token=${
            results[0].metadata.metadata.firebaseStorageDownloadTokens
          }`;
          obj.imgUrl = img;

          db.ref(`/products/${req.params.userKey}`)
            .push(obj)
            .catch(next);

          return "";
        })
        .catch(error => {
          console.error(error);
        });

      res.send("Ok");
    });

    busboy.end(req.rawBody);
  }
);

app.get(
  "/:userKey/:productKey",
  /**
   * @brief Get product with ID productKey from user with ID userKey.
   */
  (req, res, next) => {
    db.ref(`/products/${req.params.userKey}/${req.params.productKey}`)
      .once("value")
      .then(snapshot => {
        if (snapshot.exists()) {
          return res.send(snapshot.val());
        }
        return res.status(404).send("Not found!");
      })
      .catch(next);
  }
);

app.get(
  "/:userKey",
  /**
   * @brief Get all products from user with ID userKey.
   */
  (req, res, next) => {
    db.ref(`/products/${req.params.userKey}`)
      .once("value")
      .then(snapshot => {
        if (snapshot.exists()) {
          return res.send(snapshot.val());
        }
        return res.status(404).send("Not found!");
      })
      .catch(next);
  }
);

app.get(
  "/",
  /**
   * @brief Get all products of all users.
   */
  (req, res, next) => {
    db.ref(`/products`)
      .once("value")
      .then(snapshot => {
        if (snapshot.exists()) {
          return res.send(snapshot.val());
        }
        return res.status(404).send("Not found!");
      })
      .catch(next);
  }
);

app.patch(
  "/:userKey/:productKey",
  /**
   * @brief Update data of an user's product.
   */
  (req, res, next) => {
    db.ref(`/products/${req.params.userKey}/${req.params.productKey}`)
      .once("value")
      .then(snapshot => {
        if (!snapshot.exists()) {
          throw new Error("Product does not exist!");
        }
        return "";
      })
      .then(() =>
        db
          .ref(`/products/${req.params.userKey}/${req.params.productKey}`)
          .update(req.body)
      )
      .then(val => res.send(val))
      .catch(next);
  }
);

app.delete(
  "/:userKey/:productKey",
  /**
   * @brief Delete a product from a user.
   */
  (req, res, next) => {
    db.ref(`/products/${req.params.userKey}/${req.params.productKey}`)
      .remove()
      .then(() => res.sendStatus(200))
      .catch(next);
  }
);

app.delete(
  "/:userKey",
  /**
   * @brief Delete all products of an user.
   */
  (req, res, next) => {
    db.ref(`/products/${req.params.userKey}`)
      .remove(err => {
        if (err) {
          throw err;
        }
        return "";
      })
      .then(() => res.sendStatus(200))
      .catch(next);
  }
);

module.exports = app;
