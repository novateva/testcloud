const express = require("express");

const { db } = require("./common");

const app = express();

app.post(
  "/",
  /**
   * @brief Create new notification with given data
   */
  (req, res, next) => {
    if (
      !req.body.movimientoID ||
      !req.body.bolsilloDestino ||
      !req.body.usuarioIdDestino ||
      !req.body.usuarioIdOrigen
    ) {
      setImmediate(() => next(new Error("Neccessary data missing")));
      return;
    }
    db.ref(`/notifications`)
      .push(req.body)
      .then(val => res.send(val))
      .catch(next);
  }
);

app.get(
  "/:notifid",
  /**
   * @brief Get data for notification with ID notifid
   */
  (req, res, next) => {
    db.ref(`/notifications/${req.params.notifid}`)
      .once("value")
      .then(snapshot => {
        if (snapshot.exists()) {
          return res.send(snapshot.val());
        }
        return res.status(404).send("Not found!");
      })
      .catch(next);
  }
);

app.get(
  "/",
  /**
   * @brief Get data for all notifications
   * */
  (req, res, next) => {
    db.ref(`/notifications`)
      .once("value")
      .then(snapshot => {
        if (snapshot.exists()) {
          return res.send(snapshot.val());
        }
        return res.status(404).send("Not found!");
      })
      .catch(next);
  }
);

app.patch(
  "/:notifid",
  /**
   * @brief Merge a notification data with request body's data.
   */
  (req, res, next) => {
    db.ref(`/notifications/${req.params.notifid}`)
      .once("value")
      .then(snapshot => {
        if (snapshot.exists()) {
          return "";
        }
        throw new Error("Notification doesn't exist");
      })
      .then(() =>
        db.ref(`/notifications/${req.params.notifid}`).update(req.body)
      )
      .then(val => res.send(val))
      .catch(next);
  }
);

app.delete(
  "/:notifid",
  /**
   * @brief Remove a notification.
   */
  (req, res, next) => {
    db.ref(`/notifications/${req.params.notifid}`)
      .remove(err => {
        if (err) {
          throw err;
        }
        return "";
      })
      .then(() => res.sendStatus(200))
      .catch(next);
  }
);

module.exports = app;
