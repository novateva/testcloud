const express = require("express");

const { db } = require("./common");

const app = express();

app.post(
  "/:walletkey",
  /**
   * Summary. Create a pocket on a balance with request body's data.
   */
  (req, res, next) => {
    if (!req.body.bolsilloID) {
      setImmediate(() => next(new Error("Pocket has no ID!")));
      return;
    }

    db.ref(`/balance/${req.params.walletkey}`)
      .orderByChild("bolsilloID")
      .equalTo(req.body.bolsilloID)
      .limitToLast(1)
      .once("value")
      .then(snapshot => {
        if (snapshot.exists()) {
          throw new Error("Pocket already exists");
        }
        return "";
      })
      .then(() => db.ref(`/balance/${req.params.walletkey}`).push(req.body))
      .then(val => res.send(val))
      .catch(next);
  }
);

app.get(
  "/:walletkey/:randomuid",
  /**
   * Summary. Get a pocket from a balance.
   */
  (req, res, next) => {
    db.ref(`/balance/${req.params.walletkey}/${req.params.randomuid}`)
      .once("value")
      .then(snapshot => {
        if (snapshot.exists()) {
          return res.send(snapshot.val());
        }
        return res.status(404).send("Not found!");
      })
      .catch(next);
  }
);

app.get(
  "/:walletkey",
  /**
   * Summary. Get all pockets from a balance.
   */
  (req, res, next) => {
    db.ref(`/balance/${req.params.walletkey}`)
      .once("value")
      .then(snapshot => {
        if (snapshot.exists()) {
          return res.send(snapshot.val());
        }
        return res.status(404).send("Not found!");
      })
      .catch(next);
  }
);

app.get(
  "/",
  /**
   * Summary. Get all balances.
   */
  (req, res, next) => {
    db.ref(`/balance`)
      .once("value")
      .then(snapshot => {
        if (snapshot.exists()) {
          return res.send(snapshot.val());
        }
        return res.status(404).send("Not found!");
      })
      .catch(next);
  }
);

app.patch(
  "/:walletkey/:randomuid",
  /**
   * Summary. Merge a pocket from a balance with request body's data.
   */
  (req, res, next) => {
    db.ref(`/balance/${req.params.walletkey}`)
      .once("value")
      .then(snapshot => {
        if (snapshot.exists()) {
          return "";
        }
        throw new Error("Wallet does not exist!");
      })
      .then(() =>
        db
          .ref(`/balance/${req.params.walletkey}/${req.params.randomuid}`)
          .once("value")
      )
      .then(snapshot => {
        if (snapshot.exists()) {
          return "";
        }
        throw new Error("Pocket does not exist!");
      })
      .then(() =>
        db
          .ref(`/balance/${req.params.walletkey}/${req.params.randomuid}`)
          .update(req.body)
      )
      .then(val => res.send(val))
      .catch(next);
  }
);

app.delete(
  "/:walletkey/:randomuid",
  /**
   * Summary. Remove a pocket from balance.
   */
  (req, res, next) => {
    db.ref(`/balance/${req.params.walletkey}`)
      .once("value")
      .then(snapshot => {
        if (snapshot.exists()) {
          return "";
        }
        throw new Error("Wallet does not exist!");
      })
      .then(() =>
        db
          .ref(`/balance/${req.params.walletkey}/${req.params.randomuid}`)
          .remove(err => {
            console.log(err);
            if (err) {
              throw err;
            }
            return "";
          })
      )
      .then(() => res.sendStatus(200))
      .catch(next);
  }
);

app.delete(
  "/:walletkey",
  /**
   * Summary. remove a balance and all its data.
   */
  (req, res, next) => {
    db.ref(`/balance/${req.params.walletkey}`)
      .remove(err => {
        console.log(err);
        if (err) {
          throw err;
        }
        return "";
      })
      .then(() => res.sendStatus(200))
      .catch(next);
  }
);

module.exports = app;
