/* eslint-disable promise/always-return */
/* eslint-disable no-unused-expressions */
const express = require("express");

const app = express();
const { db, getUserData, UpdateUser } = require("./common");


app.get("/", (req, res, next) => {
  db.ref(`/user_rol`)
    .once("value")
    .then(snapshot => {
      if (!snapshot.exists()) {
        throw new Error("User_rol Empty!");
      }
      return res.send(snapshot.val());

    })
    .catch(next);
});

app.get("/:id", (req, res, next) => {
  db.ref(`/user_rol/${req.params.id}`)
    .once("value")
    .then(snapshot => {
      if (!snapshot.exists()) {
        throw new Error("User_rol Not Found!");
      }
      return res.send(snapshot.val());

    })
    .catch(next);
});

app.post("/:UserId", (req, res, next) => {
  db.ref(`/user_rol/${req.params.UserId}`)
    .once("value")
    .then(snapshot => {
      if (snapshot.exists()) {
        throw new Error("User_rol already registered!");
      }
      return "";
    })
    .then(() => getUserData(req.params.UserId))
    .then(snapshot => {
      if (!snapshot.exists()) {
        throw new Error("User Not Found");
      }
      const body = {
        [snapshot.val().typeAccount]: true
      };
      return db.ref(`/user_rol/${req.params.UserId}`).update(body);

    })
    .then(() => res.sendStatus(200))
    .catch(next);
});

app.patch("/:UserId", (req, res, next) => {

  const h = JSON.stringify(req.body);
  let resu = "";
  for (let i = 0; i < h.length; i += 1) {
    if (h[i] === ":" || h[i] === "}") break;

    if (h[i] !== "{" && h[i] !== '"') resu += h[i];
  }
  if (resu.length === 0) {
    setImmediate(() => next(new Error("Neccessary Role missing to update")));
    return;
  }

  db.ref(`/user_rol/${req.params.UserId}`)
    .once("value")
    .then(snapshot => {
      if (!snapshot.exists()) {
        throw new Error("User_rol Not Found!");
      }
      return "";
    })
    .then(() => getUserData(req.params.UserId))
    .then(snapshot => {
      if (!snapshot.exists()) {
        throw new Error("User Not Found");
      }

      const body = {
        typeAccount: resu
      };

      UpdateUser(req.params.UserId, body);
      db.ref(`/user_rol/${req.params.UserId}`).remove();
      return db.ref(`/user_rol/${req.params.UserId}`).update(req.body);

    })
    .then(() => res.sendStatus(200))
    .catch(next);
});

app.delete("/:UserId", (req, res, next) => {
  db.ref(`/user_rol/${req.params.UserId}`)
    .once("value")
    .then(snapshot => {
      if (!snapshot.exists()) {
        throw new Error("User_rol Not Found!");
      }
      return "";
    })

    .then(() => {
      db.ref(`/user_rol/${req.params.UserId}`).remove();
    })
    .then(() => res.sendStatus(200))
    .catch(next);
});

module.exports = app;
