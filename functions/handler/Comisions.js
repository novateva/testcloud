// eslint-disable-next-line import/no-extraneous-dependencies

const express = require("express");

const { db } = require("./common");

const app = express();

app.get("/", (req, res, next) => {
  db.ref(`/comisions`)
    .once("value")
    .then(snapshot => {
      if (snapshot.exists()) {
        return res.json(snapshot.val());
      }
      return res.status(404).json({
        ok: false,
        message: "Not found"
      });
    })
    .catch(next);
});

app.get(
  "/:comisionsKey",
  /**
   * Summary. Get all pockets from a comisions.
   */
  (req, res, next) => {
    db.ref(`/comisions/${req.params.comisionsKey}`)
      .once("value")
      .then(snapshot => {
        if (snapshot.exists()) {
          return res.send(snapshot.val());
        }
        return res.status(404).json({
          ok: false,
          message: "Not found"
        });
      })
      .catch(next);
  }
);

app.post("/", (req, res) => {
  db.ref(`/comisions`)
    .push(req.body)
    .then(val =>
      res.json({
        ok: true,
        comision: val
      })
    )
    .catch(err =>
      res.status(400).json({
        ok: false,
        err
      })
    );
});

app.patch("/:comisionsKey", (req, res) => {
  db.ref(`/comisions`)
    .child(req.params.comisionsKey)
    .once("value")
    .then(
      snapshot =>
        new Promise((resolve, reject) =>
          snapshot.exists() ? resolve(true) : reject(new Error("Not found"))
        )
    )
    // eslint-disable-next-line promise/always-return
    .then(() => {
      // eslint-disable-next-line promise/no-nesting
      db.ref(`/comisions`)
        .child(req.params.comisionsKey)
        .update(req.body)
        .then(() =>
          res.json({
            ok: true
          })
        )
        .catch(err =>
          res.status(400).json({
            ok: false,
            err: err.message
          })
        );
    })
    .catch(err =>
      res.status(404).json({
        ok: false,
        err
      })
    );
});

app.delete("/:comisionsKey", (req, res, next) => {
  db.ref(`/comisions/${req.params.comisionsKey}`)
    .remove(err => {
      if (err) {
        throw err;
      }
      return "";
    })
    .then(() =>
      res.json({
        ok: true
      })
    )
    .catch(() =>
      res.json({
        ok: false
      })
    );
});

module.exports = app;
