const express = require("express");

const { db } = require("./common");

const app = express();

app.post(
  "/:campaingKey",
  /**
   * @brief Add product to user with ID campaingKey.
   */
  (req, res, next) => {
    if (!req.params.campaingKey) {
      setImmediate(() => next(new Error("No user Key specified!")));
      return;
    }

    const busboy = new Busboy({ headers: req.headers });
    let uploadData = null;
    let origFileName;

    busboy.on("file", (fieldname, file, filename, encoding, mimetype) => {
      origFileName = filename;
      const filepath = path.join(os.tmpdir(), filename);
      uploadData = { file: filepath, type: mimetype };
      file.pipe(fs.createWriteStream(filepath));
    });

    let obj;
    // This callback will be invoked after all uploaded files are saved.
    busboy.on("finish", () => {
      obj = JSON.parse(fs.readFileSync("/tmp/wut.json", "utf8"));
      const uuid = UUID();

      bucket
        .upload(uploadData.file, {
          uploadType: "media",
          metadata: {
            metadata: {
              contentType: uploadData.type,
              firebaseStorageDownloadTokens: uuid,
              predefinedAcl: "publicRead"
            }
          }
        })
        .then(results => {
          const img = `https://firebasestorage.googleapis.com/v0/b/${
            bucket.name
          }/o/${encodeURIComponent(results[0].name)}?alt=media&token=${
            results[0].metadata.metadata.firebaseStorageDownloadTokens
          }`;
          obj.imgUrl = img;

          // eslint-disable-next-line promise/no-nesting
          db.ref(`/products/${req.params.campaingKey}`)
            .push(obj)
            .catch(next);

          return "";
        })
        .catch(error => {
          console.error(error);
        });

      res.send("Ok");
    });

    busboy.end(req.rawBody);
  }
);
app.get("/", (req, res, next) => {
  db.ref(`/campaing`)
    .once("value")
    .then(snapshot => {
      if (snapshot.exists()) {
        return res.json(snapshot.val());
      }
      return res.status(404).json({
        ok: false,
        message: "Not found"
      });
    })
    .catch(next);
});

app.get(
  "/:campaingKey",
  /**
   * Summary. Get all pockets from a campaing.
   */
  (req, res, next) => {
    db.ref(`/campaing/${req.params.campaingKey}`)
      .once("value")
      .then(snapshot => {
        if (snapshot.exists()) {
          return res.send(snapshot.val());
        }
        return res.status(404).json({
          ok: false,
          message: "Not found"
        });
      })
      .catch(next);
  }
);

app.post("/", (req, res) => {
  db.ref(`/campaing`)
    .push(req.body)
    .then(val =>
      res.json({
        ok: true,
        campaing: val
      })
    )
    .catch(err =>
      res.status(400).json({
        ok: false,
        err
      })
    );
});

app.patch("/:campaingKey", (req, res) => {
  db.ref(`/campaing`)
    .child(req.params.campaingKey)
    .once("value")
    .then(
      snapshot =>
        new Promise((resolve, reject) =>
          snapshot.exists() ? resolve(true) : reject(new Error("Not found"))
        )
    )
    // eslint-disable-next-line promise/always-return
    .then(() => {
      // eslint-disable-next-line promise/no-nesting
      db.ref(`/campaing`)
        .child(req.params.campaingKey)
        .update(req.body)
        .then(() =>
          res.json({
            ok: true
          })
        )
        .catch(err =>
          res.status(400).json({
            ok: false,
            err: err.message
          })
        );
    })
    .catch(err =>
      res.status(404).json({
        ok: false,
        err
      })
    );
});

app.delete("/:campaingKey", (req, res, next) => {
  db.ref(`/campaing/${req.params.campaingKey}`)
    .remove(err => {
      if (err) {
        throw err;
      }
      return "";
    })
    .then(() =>
      res.json({
        ok: true
      })
    )
    .catch(() =>
      res.json({
        ok: false
      })
    );
});

module.exports = app;
