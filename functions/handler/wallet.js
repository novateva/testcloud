const express = require("express");

const app = express();
const { db, getUserDataWithKey, checkUserId } = require("./common");

app.get("/", (req, res, next) => {
  db.ref(`/wallet`)
    .once("value")
    .then(snapshot => {
      if (!snapshot.exists()) {
        throw new Error("Wallet Empty!");
      }
      return res.send(snapshot.val());
    })
    .catch(next);
});

app.get("/:WalletId", (req, res, next) => {
  db.ref(`/wallet/${req.params.WalletId}`)
    .once("value")
    .then(snapshot => {
      if (!snapshot.exists()) {
        throw new Error("Wallet Not Found!");
      }
      return res.send(snapshot.val());
    })
    .catch(next);
});

app.post("/", (req, res, next) => {
  if (
    !req.body.userID ||
    !req.body.password ||
    !req.body.status ||
    !req.body.tipoBilletera
  ) {
    setImmediate(() => next(new Error("Neccessary Data missing")));
  }
  db.ref(`/wallet`)
    .orderByChild("userID")
    .equalTo(req.body.userID)
    .once("value")
    .then(snapshot => {
      if (snapshot.exists()) {
        throw new Error("User already with wallet");
      }
      return "";
    })
    .then(() => getUserDataWithKey(req.body.userID))
    .then(snapshot => {
      if (!snapshot.exists()) {
        throw new Error("User Not Found");
      }
      return "";
    })
    .then(() => db.ref(`/wallet`).push(req.body))
    .then(() => res.sendStatus(200))
    .catch(next);
});

app.post("/:ThisId", (req, res, next) => {
  if (
    !req.body.userID ||
    !req.body.password ||
    !req.body.status ||
    !req.body.tipoBilletera
  ) {
    setImmediate(() => next(new Error("Neccessary Data missing")));
  }
  db.ref(`/wallet/${req.params.ThisId}`)
    .once("value")
    .then(snapshot => {
      if (snapshot.exists()) {
        throw new Error("Id already associated with a wallet");
      }
      return "";
    })
    .then(() =>
      Promise.all([
        checkUserId(req.body.userID),
        getUserDataWithKey(req.body.userID)
      ])
    )
    .then(([CheckWallet, CheckUser]) => {
      if (CheckWallet.exists()) {
        throw new Error("User already with wallet");
      }
      if (!CheckUser.exists()) {
        throw new Error("User Not Found");
      }
      return db.ref(`/wallet/${req.params.ThisId}`).update(req.body);
    })
    .then(() => res.sendStatus(200))
    .catch(next);
});

app.patch("/:WalletID", (req, res, next) => {
  let body = {};
  db.ref(`/wallet/${req.params.WalletID}`)
    .once("value")
    .then(snapshot => {
      if (!snapshot.exists()) {
        throw new Error("Wallet Not Found");
      }
      body = snapshot.val();
      db.ref(`/wallet/${req.params.WalletID}`).remove();
      return "";
    })
    .then(() =>
      Promise.all([
        checkUserId(req.body.userID),
        getUserDataWithKey(req.body.userID)
      ])
    )
    .then(([CheckWallet, CheckUser]) => {
      if (CheckWallet.exists()) {
        db.ref(`/wallet/${req.params.WalletID}`).update(body);
        throw new Error("User already with wallet");
      }
      if (!CheckUser.exists()) {
        db.ref(`/wallet/${req.params.WalletID}`).update(body);
        throw new Error("User Not Found");
      }
      return db.ref(`/wallet/${req.params.WalletID}`).update(req.body);
    })
    .then(() => res.sendStatus(200))
    .catch(next);
});

app.delete("/:WalletId", (req, res, next) => {
  db.ref(`/wallet/${req.params.WalletId}`)
    .once("value")
    .then(snapshot => {
      if (!snapshot.exists()) {
        throw new Error("Wallet Not Found!");
      }
      return "";
    })
    .then(() => db.ref(`/wallet/${req.params.WalletId}`).remove())
    .then(() => res.sendStatus(200))
    .catch(next);
});

module.exports = app;
