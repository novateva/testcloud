const express = require("express");

const { db } = require("./common");

const app = express();

app.post("/", (req, res, next) => {
  if (
    !req.body.bolsilloId ||
    !req.body.idClub ||
    !req.body.idProducto ||
    !req.body.userId ||
    !req.body.userOwnerId
  ) {
    setImmediate(() => next(new Error("Neccessary data missing")));
    return;
  }
  db.ref(`/peticion`)
    .then(() => db.ref(`/peticion`).push(req.body))
    .then(val => res.send(val))
    .catch(next);
});

app.get("/:reqid", (req, res, next) => {
  db.ref(`/peticion/${req.params.reqid}`)
    .once("value")
    .then(snapshot => {
      if (snapshot.exists()) {
        return res.send(snapshot.val());
      }
      return res.status(404).send("Not found!");
    })
    .catch(next);
});

app.get("/", (req, res, next) => {
  db.ref(`/peticion`)
    .once("value")
    .then(snapshot => {
      if (snapshot.exists()) {
        return res.send(snapshot.val());
      }
      return res.status(404).send("Not found!");
    })
    .catch(next);
});

app.patch("/:reqid", (req, res, next) => {
  db.ref(`/peticion/${req.params.reqid}`)
    .once("value")
    .then(snapshot => {
      if (snapshot.exists()) {
        return "";
      }
      throw new Error("Request doesn't exist");
    })
    .then(() => db.ref(`/peticion/${req.params.reqid}`).update(req.body))
    .then(val => res.send(val))
    .catch(next);
});

app.delete("/:reqid", (req, res, next) => {
  db.ref(`/peticion/${req.params.reqid}`)
    .remove(err => {
      if (err) {
        throw err;
      }
      return "";
    })
    .then(() => res.sendStatus(200))
    .catch(next);
});

module.exports = app;
