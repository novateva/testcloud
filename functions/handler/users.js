const express = require("express");
const Busboy = require("busboy");
const path = require("path");
const fs = require("fs");
const UUID = require("uuid-v4");
const os = require("os");
const { db, bucket } = require("./common");

const app = express();

app.get("/", (req, res, next) => {
  db.ref(`/users`)
    .once("value")
    .then(snapshot => {
      if (!snapshot.exists()) {
        throw new Error("Users database empty!");
      }
      return res.send(snapshot.val());
    })
    .catch(next);
});

app.get("/:UserID", (req, res, next) => {
  db.ref(`/users/${req.params.UserID}`)
    .once("value")
    .then(snapshot => {
      if (!snapshot.exists()) {
        throw new Error("User Not Found");
      }
      return res.send(snapshot.val());
    })
    .catch(next);
});

app.post("/", (req, res, next) => {
  if (
    !req.body.name ||
    !req.body.password ||
    !req.body.phone ||
    !req.body.typeAccount ||
    !req.body.userId ||
    !req.body.imgUser ||
    !req.body.birthDay ||
    !req.body.gender
  ) {
    setImmediate(() => next(new Error("Neccessary Data missing")));
  }
  const busboy = new Busboy({ headers: req.headers });
  let uploadData = null;
  let origFileName;

  busboy.on("file", (fieldname, file, filename, encoding, mimetype) => {
    origFileName = filename;
    const filepath = path.join(os.tmpdir(), filename);
    uploadData = { file: filepath, type: mimetype };
    file.pipe(fs.createWriteStream(filepath));
  });

  let obj;
  // This callback will be invoked after all uploaded files are saved.
  busboy.on("finish", () => {
    obj = JSON.parse(fs.readFileSync("/tmp/wut.json", "utf8"));
    const uuid = UUID();

    bucket
      .upload(uploadData.file, {
        uploadType: "media",
        metadata: {
          metadata: {
            contentType: uploadData.type,
            firebaseStorageDownloadTokens: uuid,
            predefinedAcl: "publicRead"
          }
        }
      })
      .then(results => {
        const img = `https://firebasestorage.googleapis.com/v0/b/${
          bucket.name
        }/o/${encodeURIComponent(results[0].name)}?alt=media&token=${
          results[0].metadata.metadata.firebaseStorageDownloadTokens
        }`;
        obj.imgUser = img;

        db.ref(`/users`)
          .push(obj)
          .catch(next);

        return "";
      })
      .catch(error => {
        console.error(error);
      });

    res.send("Ok");
  });

  busboy.end(req.rawBody);
});

app.post("/:ThisId", (req, res, next) => {
  if (
    !req.body.name ||
    !req.body.password ||
    !req.body.phone ||
    !req.body.typeAccount ||
    !req.body.userId ||
    !req.body.imgUser ||
    !req.body.birthDay ||
    !req.body.gender
  ) {
    setImmediate(() => next(new Error("Neccessary Data missing")));
  }

  const busboy = new Busboy({ headers: req.headers });
  let uploadData = null;
  let origFileName;

  busboy.on("file", (fieldname, file, filename, encoding, mimetype) => {
    origFileName = filename;
    const filepath = path.join(os.tmpdir(), filename);
    uploadData = { file: filepath, type: mimetype };
    file.pipe(fs.createWriteStream(filepath));
  });

  let obj;
  // This callback will be invoked after all uploaded files are saved.
  busboy.on("finish", () => {
    obj = JSON.parse(fs.readFileSync("/tmp/wut.json", "utf8"));
    const uuid = UUID();

    bucket
      .upload(uploadData.file, {
        uploadType: "media",
        metadata: {
          metadata: {
            contentType: uploadData.type,
            firebaseStorageDownloadTokens: uuid,
            predefinedAcl: "publicRead"
          }
        }
      })
      .then(results => {
        const img = `https://firebasestorage.googleapis.com/v0/b/${
          bucket.name
        }/o/${encodeURIComponent(results[0].name)}?alt=media&token=${
          results[0].metadata.metadata.firebaseStorageDownloadTokens
        }`;
        obj.imgUser = img;

        db.ref(`/users/${req.params.ThisId}`)
          .update(obj)
          .catch(next);

        return "";
      })
      .catch(error => {
        console.error(error);
      });

    res.send("Ok");
  });

  busboy.end(req.rawBody);
});

app.patch("/:UserId", (req, res, next) => {
  db.ref(`/users/${req.params.UserId}`)
    .once("value")
    .then(snapshot => {
      if (!snapshot.exists()) {
        throw new Error("User Not Found!");
      }
      return "";
    })
    .then(() => db.ref(`/users/${req.params.UserId}`).update(req.body))
    .then(() => res.sendStatus(200))
    .catch(next);
});

app.delete("/:UserId", (req, res, next) => {
  db.ref(`/users/${req.params.UserId}`)
    .once("value")
    .then(snapshot => {
      if (!snapshot.exists()) {
        throw new Error("User Not Found!");
      }
      return "";
    })
    .then(() => db.ref(`/users/${req.params.UserId}`).remove())
    .then(() => res.sendStatus(200))
    .catch(next);
});

module.exports = app;
