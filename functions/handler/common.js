const { db, admin, bucket } = require("../Config");



const UpdateUser = (key, body) => db.ref(`/users/${key}`).update(body);

const functions = require("firebase-functions");
const express = require("express");


const getUserData = key =>
  db
    .ref(`/users`)
    .orderByKey()
    .equalTo(key)
    .once("value");


function rootHandlerWrapper(expressApp) {
  return (
    /**
     * @param {express.Request} req
     * @param {express.Response} res
     * @return {functions.HttpsFunction}
     */
    (req, res) => {
      if (!req.path) {
        req.url = `/${req.url}`;
      }

      return expressApp(req, res);
    }
  );
}


/**
 * @param {(String|Number)} key
 * @returns {Promise<admin.database.DataSnapshot>} firebase snapshot.
 */

function getUserDataWithKey(key) {
  return db
    .ref(`/users`)
    .orderByKey()
    .equalTo(key)
    .once("value");

}

const checkUserId = key =>
  db
    .ref(`/wallet`)
    .orderByChild("userID")
    .equalTo(key)
    .once("value");


module.exports = {
  admin,
  UpdateUser,
  db,
  rootHandlerWrapper,
  getUserData,
  getUserDataWithKey,
  checkUserId,
  bucket

};
