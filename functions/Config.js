const admin = require("firebase-admin");

const config = {
  apiKey: "AIzaSyDEluFSAtP3pWCKojHdFTI0HpmJp2kjuqE",
  authDomain: "test2-4f20b.firebaseapp.com",
  databaseURL: "https://test2-4f20b.firebaseio.com",
  projectId: "test2-4f20b",
  storageBucket: "test2-4f20b.appspot.com",
  messagingSenderId: "822302011423"
};

admin.initializeApp(config);
const db = admin.database();
const bucket = admin.storage().bucket();
module.exports = {
  admin,
  db,
  bucket
};
