const functions = require("firebase-functions");

const { rootHandlerWrapper } = require("./handler/common");


const userRol = require("./handler/userRol");
const wallet = require("./handler/wallet");
const users = require("./handler/users");

const notifications = require("./handler/notification");
const peticion = require("./handler/peticion");
const products = require("./handler/products");

const friends = require("./handler/Friends");
const balances = require("./handler/Balances");
const comisions = require("./handler/Comisions");
const bolsillos = require("./handler/Bolsillos");
const movimiento = require("./handler/Movimiento");
const campaing = require("./handler/Campaing");



exports.notifications = functions.https.onRequest(notifications);
exports.peticion = functions.https.onRequest(peticion);
exports.products = functions.https.onRequest(products);
exports.userRol = functions.https.onRequest(userRol);
exports.wallet = functions.https.onRequest(wallet);
exports.users = functions.https.onRequest(users);
exports.friends = functions.https.onRequest(rootHandlerWrapper(friends));
exports.bolsillos = functions.https.onRequest(rootHandlerWrapper(bolsillos));
exports.balances = functions.https.onRequest(rootHandlerWrapper(balances));
exports.comisions = functions.https.onRequest(rootHandlerWrapper(comisions));
exports.movimiento = functions.https.onRequest(rootHandlerWrapper(movimiento));
exports.campaing = functions.https.onRequest(rootHandlerWrapper(campaing));
